﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Xml;

namespace WebApplication1
{
    /// <summary>
    /// Handler1 的摘要说明
    /// </summary>
    public class Handler1 : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            context.Response.ContentType = "application/json";
            StreamReader st1 = new StreamReader(context.Request.InputStream);
            string str2 = st1.ReadToEnd();
            Console.WriteLine(str2);
            string jsonText = str2;
            string SystemCode = null;
            
            //创建xml对象
            XmlDocument myXML = new XmlDocument();
            XmlNode goods = myXML.CreateNode(XmlNodeType.Element, "goods", null);
            myXML.AppendChild(goods);
            XmlNode price = myXML.CreateNode(XmlNodeType.Element, "price", null);
            price.InnerText = "9784564  E3" +
                "3QWA";
            goods.AppendChild(price);
            //创建文件对象
            MemoryStream stream = new MemoryStream();
            myXML.Save(stream);
            byte[] byts = new byte[(int)stream.Length];
            //创建json对象
            JObject jObject = new JObject();
            jObject.Add("name","123");
            jObject.Add("file", byts);

            /*JObject jo = (JObject)JsonConvert.DeserializeObject(jsonText);
            
              SystemCode = jo["SystemCode"].ToString();
            \"PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTE2Ij8 + DQo8UHJvZHVjdE1hcmtJbmZvIHhtbG5zOnhzaT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEtaW5zdGFuY2UiIHhtbG5zOnhzZD0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEiPg0KICA8SUQ + NzY4ZmI3MTgtZmFjZS00OGNiLWFhYzAtZDhjNmM0NzcyNjNjPC9JRD4NCiAgPENvbnRhaW5lck5hbWU + MTAtMjAyMDEyLTAwMDYo6Lef6Liq5Y2h5Y + 3KTwvQ29udGFpbmVyTmFtZT4NCiAgPFByb2R1Y3ROYW1lPkRHLTAwMDEo5Zu + 5Y + 3KTwvUHJvZHVjdE5hbWU + DQogIDxEZXNjcmlwdGlvbj7lr7znrqExMijkuqflk4HlkI3np7ApPC9EZXNjcmlwdGlvbj4NCiAgPFByb2R1Y3RObz4wMDko5Lqn5ZOB5bqP5Y + 3KTwvUHJvZHVjdE5vPg0KICA8dGNobm8 + VEMwMDEo5Y + w5qyh5Y + 3KTwvdGNobm8 + DQogIDxTdGF0dXM + MCjnirbmgIEw77ya5pyq5omT5qCHKTwvU3RhdHVzPg0KPC9Qcm9kdWN0TWFya0luZm8 + \"
            */
                string str = "{\"Flag\":true,\"Message\":\"成功获取所有任务文件 \",\"Data\":[{\"FileName\":\"10 - 202012 - 0093___PM___202012141701595644223_1.xml\",\"FileType\":\"xml\",\"FileInfo\":[255,254,60,0,63,0,120,0,109,0,108,0,32,0,118,0,101,0,114,0,115,0,105,0,111,0,110,0,61,0,34,0,49,0,46,0,48,0,34,0,32,0,101,0,110,0,99,0,111,0,100,0,105,0,110,0,103,0,61,0,34,0,117,0,116,0,102,0,45,0,49,0,54,0,34,0,63,0,62,0,13,0,10,0,60,0,80,0,114,0,111,0,100,0,117,0,99,0,116,0,77,0,97,0,114,0,107,0,73,0,110,0,102,0,111,0,32,0,120,0,109,0,108,0,110,0,115,0,58,0,120,0,115,0,105,0,61,0,34,0,104,0,116,0,116,0,112,0,58,0,47,0,47,0,119,0,119,0,119,0,46,0,119,0,51,0,46,0,111,0,114,0,103,0,47,0,50,0,48,0,48,0,49,0,47,0,88,0,77,0,76,0,83,0,99,0,104,0,101,0,109,0,97,0,45,0,105,0,110,0,115,0,116,0,97,0,110,0,99,0,101,0,34,0,32,0,120,0,109,0,108,0,110,0,115,0,58,0,120,0,115,0,100,0,61,0,34,0,104,0,116,0,116,0,112,0,58,0,47,0,47,0,119,0,119,0,119,0,46,0,119,0,51,0,46,0,111,0,114,0,103,0,47,0,50,0,48,0,48,0,49,0,47,0,88,0,77,0,76,0,83,0,99,0,104,0,101,0,109,0,97,0,34,0,62,0,13,0,10,0,32,0,32,0,60,0,73,0,68,0,62,0,49,0,48,0,45,0,50,0,48,0,50,0,48,0,49,0,50,0,45,0,48,0,48,0,57,0,51,0,95,0,50,0,48,0,50,0,48,0,45,0,48,0,48,0,49,0,49,0,45,0,48,0,51,0,49,0,49,0,60,0,47,0,73,0,68,0,62,0,13,0,10,0,32,0,32,0,60,0,67,0,111,0,110,0,116,0,97,0,105,0,110,0,101,0,114,0,78,0,97,0,109,0,101,0,62,0,49,0,48,0,45,0,50,0,48,0,50,0,48,0,49,0,50,0,45,0,48,0,48,0,57,0,51,0,60,0,47,0,67,0,111,0,110,0,116,0,97,0,105,0,110,0,101,0,114,0,78,0,97,0,109,0,101,0,62,0,13,0,10,0,32,0,32,0,60,0,80,0,114,0,111,0,100,0,117,0,99,0,116,0,78,0,97,0,109,0,101,0,62,0,70,0,49,0,66,0,48,0,51,0,49,0,49,0,45,0,48,0,60,0,47,0,80,0,114,0,111,0,100,0,117,0,99,0,116,0,78,0,97,0,109,0,101,0,62,0,13,0,10,0,32,0,32,0,60,0,68,0,101,0,115,0,99,0,114,0,105,0,112,0,116,0,105,0,111,0,110,0,62,0,185,112,107,112,239,141,85,83,17,84,0,150,250,81,227,83,252,91,161,123,60,0,47,0,68,0,101,0,115,0,99,0,114,0,105,0,112,0,116,0,105,0,111,0,110,0,62,0,13,0,10,0,32,0,32,0,60,0,80,0,114,0,111,0,100,0,117,0,99,0,116,0,78,0,111,0,62,0,50,0,48,0,50,0,48,0,45,0,48,0,48,0,49,0,49,0,45,0,48,0,51,0,49,0,49,0,60,0,47,0,80,0,114,0,111,0,100,0,117,0,99,0,116,0,78,0,111,0,62,0,13,0,10,0,32,0,32,0,60,0,116,0,99,0,104,0,110,0,111,0,62,0,71,0,45,0,49,0,50,0,49,0,49,0,45,0,48,0,48,0,52,0,60,0,47,0,116,0,99,0,104,0,110,0,111,0,62,0,13,0,10,0,32,0,32,0,60,0,83,0,116,0,97,0,116,0,117,0,115,0,62,0,49,0,60,0,47,0,83,0,116,0,97,0,116,0,117,0,115,0,62,0,13,0,10,0,60,0,47,0,80,0,114,0,111,0,100,0,117,0,99,0,116,0,77,0,97,0,114,0,107,0,73,0,110,0,102,0,111,0,62,0,0]}]}";
            context.Response.Flush();
            context.Response.Write(str);
            
        }
        
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}